void callback(char* topic, byte* payload, unsigned int length) {
  String str = "";
  if (debug)Serial.print("Message arrived [");
  if (debug)Serial.print(topic);
  if (debug)Serial.print("] ");
  if (debug) Serial.print(length);
  if (debug) Serial.print("payload=");

  for (int i = 0; i < length; i++) {
    if (debug) Serial.print((char)payload[i]);
    str += (char)payload[i];
  }
  if (debug) Serial.println();

  /*
    client.subscribe("ba/ltbs");
      client.subscribe("ba/ltbk");
      client.subscribe("ba/lobs");
      client.subscribe("ba/lobk");
  */

  if ((String)topic == "ba/ltbs") {
    if (debug)Serial.println(str.toInt());
    if (writeEEPROM(str.toInt(), light_time_base_adr)) light_time_base = str.toInt();
  }

  if ((String)topic == "ba/ltbk") {
    if (debug)Serial.println(str.toInt());
    if ( writeEEPROM(str.toInt(), light_time_back_adr)) light_time_back = str.toInt();
  }

  if ((String)topic == "ba/lobs") {
    if (debug)Serial.println(str.toInt());
    if ( writeEEPROM(str.toInt(), light_on_base_adr)) light_on_base = str.toInt();
  }

  if ((String)topic == "ba/lobk") {
    if (debug)Serial.println(str.toInt());
    if ( writeEEPROM(str.toInt(), light_on_back_adr)) light_on_back = str.toInt();
  }

/*
  if (debug) {
    if ((String)topic == "ba/tstl") {
      if (debug)Serial.println(str.toInt());
      mcp.digitalWrite(CO2, false);//инв логика
      mcp.digitalWrite(vent_left, true);
      delay(30000);
      mcp.digitalWrite(light_left, true);//инв логика
      mcp.digitalWrite(vent_left, false);
    }
    if ((String)topic == "ba/tstc") {
      if (debug)Serial.println(str.toInt());

      mcp.digitalWrite(light_center, false);//инв логика
      mcp.digitalWrite(vent_center, true);
      delay(30000);
      mcp.digitalWrite(light_center, true);//инв логика
      mcp.digitalWrite(vent_center, false);
    }
    if ((String)topic == "ba/tstr") {
      if (debug)Serial.println(str.toInt());
      mcp.digitalWrite(light_right, true);
      mcp.digitalWrite(vent_right, true);
      delay(30000);
      mcp.digitalWrite(light_right, false);
      mcp.digitalWrite(vent_right, false);
    }
  }
  */
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress & address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}

time_t getNtpTime()
{
  IPAddress ntpServerIP; // NTP server's ip address
  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  Serial.println("Transmit NTP Request");
  // get a random server from the pool
  Udp.begin(localPort);
  //WiFi.hostByName(ntpServerName, ntpServerIP);
  //Serial.print(ntpServerName);
  Serial.print(": ");
  Serial.println(timeServer);
  sendNTPpacket(timeServer);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
      Udp.flush();
      Udp.stop();
    }
  }
  Serial.println("No NTP Response :-(");
  return 0; // return 0 if unable to get the time
}

boolean reconnect() {
  if (debug) Serial.println("Reconnect");
  if (client.connect("BigAquaLight", mqtt_user, mqtt_pass)) {
    if (debug) Serial.println("MQQT Connection UP");

    client.subscribe("ba/ltbs");
    client.subscribe("ba/ltbk");
    client.subscribe("ba/lobs");
    client.subscribe("ba/lobk");
    client.subscribe("ba/tstl");
    client.subscribe("ba/tstc");
    client.subscribe("ba/tstr");
  }
  return client.connected();
}

void mqqt_check() {
  //if (debug) Serial.print("MQQT");
  if (!client.connected()) {
    if (millis() - lastReconnectAttempt > 5000) {
      if (debug) Serial.println(millis());
      lastReconnectAttempt = millis();
      if (reconnect()) {
        lastReconnectAttempt = 0;
      }
    }
  } else {
    client.loop();
  }
}

void lastdata_publish(time_t time_pump) {
  if (debug) Serial.println("Publish_date");
  String str;
  str += day(time_pump);
  str += '.';
  str += month(time_pump);
  str += '.';
  str += year(time_pump);
  str += '/';
  str += hour(time_pump);
  str += ':';
  str += minute(time_pump);
  str += '/';
  str += weekday(time_pump);
  str.toCharArray(message, 20);
  if (debug) Serial.println(message);
  client.publish("ba/curTime", message);
}

void interval_base_publish() {
  if (debug) Serial.println("Publish_interval");
  String str;
  str += "T";
  str += light_time_base;
  str += " Ton";
  str += light_on_base;
  str.toCharArray(message, 10);
  if (debug) Serial.println(message);
  client.publish("ba/base", message);
}

void interval_back_publish() {
  if (debug) Serial.println("Publish_interval");
  String str;
  str += 'T';
  str += light_time_back;
  str += " Ton";
  str += light_on_back;
  str.toCharArray(message, 10);
  if (debug) Serial.println(message);
  client.publish("ba/back", message);
}
