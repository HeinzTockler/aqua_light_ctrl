///http://arduino.esp8266.com/stable/package_esp8266com_index.json
/*
 * Железка управляет светом в большом аквасе.
 * 
 * Расширитель портов управляет исполнительными механизмами
 * 
 * Первое реле управляет основным светом
 * Второе реле управляет соленоидом на балоне огнетушителя
 * 
 *  кулерами для охлаждения основных светильников
 *
  Кнопка принудительно включает свет
*/
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <TimeLib.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <PubSubClient.h>
#include <EEPROM.h>
#include <Wire.h>
#include "Adafruit_MCP23017.h"

#ifndef STASSID
#define STASSID ""
#define STAPSK  ""
#endif

const char* ssid = STASSID;
const char* password = STAPSK;

//////////Настройки MQTT//////////////////////////////////////////////////////
const char *mqtt_server = "192.168.11.4"; // Имя сервера MQTT
const int mqtt_port = 1883; // Порт для подключения к серверу MQTT
const char *mqtt_user = "heinz"; // Логи для подключения к серверу MQTT
const char *mqtt_pass = "192422901"; // Пароль для подключения к серверу MQTT

WiFiClient wclient;
PubSubClient client(wclient);

//////////NTP//////////////////////////////////////////////////////
IPAddress timeServer(192, 168, 1, 2);
const int timeZone = 8;
const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets
WiFiUDP Udp;
unsigned int localPort = 2390;      // local port to listen for UDP packets

bool debug = false;
char message[30]; //сюда кэшируем дату

//////Входа///////////////////////////////////////////////////////////////////
/*
  byte relay_1 = 5; //реле основного света
  byte relay_2 = 14; //реле подсветки
  byte relay_3 = 12; //реле вентилятора
  byte man_1 = 4; //вход для включения подсветки
  byte man_2 = 13; //вход для включения света
   21  GPA0  0
    22  GPA1  1
    23  GPA2  2
    24  GPA3  3
    25  GPA4  4
    26  GPA5  5
    27  GPA6  6
    28  GPA7  7
*/
byte man_1 = 12; //вход для включения подсветки D6
byte man_2 = 13; //вход для включения света D7

#define light_center  0 //реле основного света
#define CO2  1
#define light_right  2 
#define vent_center  3
#define vent_left  4
#define vent_right  5
#define back_light  6

//Время работы освещения в часах
int light_time_base = 6;
int light_time_back = 6;
int light_on_base = 10;
int light_on_back = 10;


///////Переменные/////////////////////////////////////////////////////////////
unsigned long currentTime = 0; //для организации циклов прерывания
unsigned long publicTime = 0; //для организации циклов прерывания
unsigned long fanTime = 0; //для организации циклов прерывания
unsigned int cicle_update = 1000; //основной цикл минуты
unsigned long cicle_public = 60000; // цикл для публикации данных
unsigned long lastReconnectAttempt = 0;
unsigned long min_cooling = 300000; //охлаждаем светильник 5 минут
//bool need_fan = false;
//bool lb_on = false;
bool man_base = false;
bool man_back = false;

Adafruit_MCP23017 mcp;
/*
  int curTime[2] = {
  0, 0
  };*/

void setup() {
  Serial.begin(115200);
  Serial.println("Booting");

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname("LightCtrl_big");

  // No authentication by default
  // ArduinoOTA.setPassword("admin");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });

  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });

  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });

  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  /*
    Physical Pin #  Pin Name  Pin ID
    21  GPA0  0
    22  GPA1  1
    23  GPA2  2
    24  GPA3  3
    25  GPA4  4
    26  GPA5  5
    27  GPA6  6
    28  GPA7  7
    1 GPB0  8
    2 GPB1  9
    3 GPB2  10
    4 GPB3  11
    5 GPB4  12
    6 GPB5  13
    7 GPB6  14
    8 GPB7  15
  */

  mcp.begin();      // use default address 0
  mcp.pinMode(light_center, OUTPUT);
  mcp.pinMode(CO2, OUTPUT);
  mcp.pinMode(light_right, OUTPUT);
  mcp.pinMode(vent_center, OUTPUT);
  mcp.pinMode(vent_left, OUTPUT);
  mcp.pinMode(vent_right, OUTPUT);
  mcp.pinMode(back_light, OUTPUT);

  //pinMode(relay_1, OUTPUT);
  //pinMode(relay_2, OUTPUT);
  //pinMode(relay_3, OUTPUT);
  pinMode(man_1, INPUT);
  pinMode(man_2, INPUT);

  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);

  setSyncProvider(getNtpTime);
  setSyncInterval(4000);//интервал синхронизации - 1,5 часов
  checkEEPROM();//смотрим че там в ПЗУ, если что обновляем

  currentTime = millis();
  publicTime = millis();

  if (debug) Serial.println("Setup_end");
}

void loop() {
  ArduinoOTA.handle();

  mqqt_check();
  //основной цикл 3сек
  if (millis() > (currentTime + cicle_update)) {
    currentTime = millis();
    manual();
    Main();
  } else if (currentTime > millis()) {
    currentTime = millis();
    Serial.println("Overflov cicle_update");
  }

  // цикл для обновления данных
  if (millis() > (publicTime + cicle_public)) {
    publicTime = millis();
    wifi_update();
    lastdata_publish(now());
    interval_base_publish();
    interval_back_publish();
  } else if (publicTime > millis()) {
    publicTime = millis();
    Serial.println("Overflov cicle_hour");
  }
/*
  if (need_fan) {
    if (millis() > (fanTime + min_cooling)) {
      //сбрасываем флаг и выключаем обдув
      need_fan = false;
      digitalWrite(relay_3, false);
    } else if (fanTime > millis()) {
      fanTime = millis();
      Serial.println("Overflov cicle_update");
    }
  }*/
}
