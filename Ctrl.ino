void Main() {
  //if (debug)Serial.println("main");
  //getTime();
  /*
    #define light_center = 0 //реле основного света инв логика
    #define CO2 = 1 инв логика
    #define light_right = 2
    #define vent_center = 3
    #define vent_left = 4
    #define vent_right = 5
    #define back_light = 6
  */

  if (man_base) {
    mcp.digitalWrite(light_center, false);
    mcp.digitalWrite(vent_center, true);
    mcp.digitalWrite(CO2, false);//инв логика
    // mcp.digitalWrite(vent_left, true);
    mcp.digitalWrite(light_right, true);
    // mcp.digitalWrite(vent_right, true);
  } else {
    //Сначала светим левым полчаса, потом включаем правый полчаса, потом боковыми и центальным,
    //потом выключаем левый, потом выключаем центральный, потол выключаем все.
    //в зависимости от времени включаем необходимые выхода
    if (hour() >= light_on_base && hour() < (light_on_base + light_time_base) ) {
      /*if (minute() <= 30 && hour() == light_on_base) {
        //включаем сначала левый
        mcp.digitalWrite(CO2, false);//инв логика
        // mcp.digitalWrite(vent_left, true);

        // mcp.digitalWrite(light_right, false);
        mcp.digitalWrite(light_center, true);//инв логика

        //mcp.digitalWrite(vent_right, false);
        mcp.digitalWrite(vent_center, false);
        }*/
      /*
            if (minute() > 30 && hour() == light_on_base) {
              //включаем правый и левый
              mcp.digitalWrite(light_right, true);
              mcp.digitalWrite(vent_right, true);

              mcp.digitalWrite(CO2, false);//инв логика
              mcp.digitalWrite(vent_left, true);

              mcp.digitalWrite(light_center, true);//инв логика
              mcp.digitalWrite(vent_center, false);
            }*/

      // if ( hour() >= light_on_base + 1 && hour() < (light_on_base + (light_time_base - 1))) {
      //включаем все
      mcp.digitalWrite(light_right, true);
       mcp.digitalWrite(vent_right, true);

      mcp.digitalWrite(CO2, false);//инв логика
      //mcp.digitalWrite(vent_left, true);

     mcp.digitalWrite(light_center, false);//инв логика
      mcp.digitalWrite(vent_center, true);
      // }

      /*
            if ( hour() == (light_on_base + (light_time_base - 1)) && minute() <= 30) {
              //выключаем левый
              mcp.digitalWrite(light_right, true);
              mcp.digitalWrite(vent_right, true);

              mcp.digitalWrite(CO2, true);//инв логика
              mcp.digitalWrite(vent_left, false);

              mcp.digitalWrite(light_center, false);//инв логика
              mcp.digitalWrite(vent_center, true);
            }*/
      /*
            if ( hour() == (light_on_base + (light_time_base - 1)) && minute() > 30) {
              //выключаем центральный
              mcp.digitalWrite(light_right, true);
              mcp.digitalWrite(vent_right, true);

              mcp.digitalWrite(CO2, true);//инв логика
              mcp.digitalWrite(vent_left, false);

              mcp.digitalWrite(light_center, true);//инв логика
              mcp.digitalWrite(vent_center, false);
            }*/

    }
    //в конце периода света выключаем все
    if (hour() >= light_on_base + light_time_base) {
      mcp.digitalWrite(light_right, false);
      mcp.digitalWrite(CO2, true);
      mcp.digitalWrite(light_center, true);
      mcp.digitalWrite(vent_right, false);
      mcp.digitalWrite(vent_left, false);
      mcp.digitalWrite(vent_center, false);
    }

    //до начала периода света всю глушим
    if (hour() < light_on_base) {
      mcp.digitalWrite(light_right, false);
      mcp.digitalWrite(vent_right, false);
      mcp.digitalWrite(CO2, true);
      mcp.digitalWrite(vent_left, false);
      mcp.digitalWrite(light_center, true);
      mcp.digitalWrite(vent_center, false);
    }
  }

  //если есть команда на ручное включение то включаем
  if (man_back) {
    mcp.digitalWrite(back_light, true);
    // if (debug)Serial.println("manual back");
  } else {
    if (hour() >= light_on_back && hour() < light_on_back + light_time_back ) {
      mcp.digitalWrite(back_light, true);
    }

    //в конце периода или перед его началом выключаем все
    if (hour() >= light_on_back + light_time_back)
      mcp.digitalWrite(back_light, false);

    if (hour() < light_on_back)
      mcp.digitalWrite(back_light, false);
  }
}

/*
  void digitalClockDisplay()
  {
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(' ');
  Serial.print(day());
  Serial.print(' ');
  Serial.print(month());
  Serial.print(' ');
  Serial.print(year());
  Serial.println();
  }

  void printDigits(int digits)
  {
  // utility function for digital clock display: prints preceding colon and leading 0
  Serial.print(':');
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
  }

  //метод для получения текущего времени
  void getTime() {
  curTime[0] = hour();
  curTime[1] = minute();
  // if (debug)digitalClockDisplay();
  }
*/

void manual() {
  man_back = digitalRead(man_1);
  //if (debug)Serial.print("man_back= ");
  // if (debug)Serial.println(man_back);
  man_base = digitalRead(man_2);
  // if (debug)Serial.print("man_base= ");
  // if (debug)Serial.println(man_base);
}
