///адреса хранения данных о поливе
#define light_time_base_adr 0
#define light_time_back_adr (light_time_base_adr+sizeof(int))
#define light_on_base_adr (light_time_back_adr+sizeof(int))
#define light_on_back_adr (light_on_base_adr+sizeof(int))

void checkEEPROM() {
  int fl1 = 255;
  int fl2 = 255;
  int fl3 = 255;
  int fl4 = 255;

  EEPROM.begin(50);
  EEPROM.get(light_time_base_adr, fl1);
  EEPROM.get(light_time_back_adr, fl2);
  EEPROM.get(light_on_base_adr, fl3);
  EEPROM.get(light_on_back_adr, fl4);

  if (debug) Serial.print("light_time_base= ");
  if (debug) Serial.print(fl1);
  if (debug) Serial.println(" ");

  if (debug) Serial.print("light_time_back= ");
  if (debug) Serial.print(fl2);
  if (debug) Serial.println(" ");

  if (debug) Serial.print("light_on_base_adr= ");
  if (debug) Serial.print(fl3);
  if (debug) Serial.println(" ");

  if (debug) Serial.print("light_on_back_adr= ");
  if (debug) Serial.print(fl4);
  if (debug) Serial.println(" ");

  //  if (debug)Serial.println(message);

  //кусок тупого кода, не хотелось с массивом заморачиваться
  if (fl1 != light_time_base & fl1 != 0 & fl1 != 255 & fl1 > 0)light_time_base = fl1;
  if (fl2 != light_time_back & fl2 != 0 & fl2 != 255 & fl2 > 0)light_time_back = fl2;
  if (fl3 != light_on_base & fl3 != 0 & fl3 != 255 & fl3 > 0)light_on_base = fl3;
  if (fl4 != light_on_back & fl4 != 0 & fl4 != 255 & fl4 > 0)light_on_back = fl4;

  EEPROM.end();
}

bool writeEEPROM(int val, byte adr) {
  int param = 0;
  if (debug) Serial.print("adr= ");
  if (debug) Serial.print(adr);
  if (debug) Serial.println(" ");
  EEPROM.begin(25);
  EEPROM.get(adr, param);
  if (debug) Serial.print("param= ");
  if (debug) Serial.print(param);
  if (debug) Serial.println(" ");
  if (param != val) {
    EEPROM.put(adr, val);
    if (debug) Serial.print("write ");
    if (debug) {
      EEPROM.get(adr, param);
      Serial.println(param);
    }
    EEPROM.commit();
    return true;
  } else return false;
  EEPROM.end();
}

bool writeEEPROM_str(String val, byte adr) {
  String param ;
  EEPROM.begin(25);
  EEPROM.get(adr, param);
  if (debug) Serial.println(param);
  if (param != val) {
    EEPROM.put(adr, val);
    if (debug) Serial.print("write ");
    if (debug) {
      EEPROM.get(adr, param);
      Serial.println(param);
    }
    EEPROM.commit();
    return true;
  } else return false;
  EEPROM.end();
}
